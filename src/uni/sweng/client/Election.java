package uni.sweng.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Election implements EntryPoint{

	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network " + "connection and try again.";

	private final ElectionServiceAsync electionService = GWT.create(ElectionService.class);
	

	public void onModuleLoad() {
		
		RootPanel.get().clear();
		
		final Button loginButton = new Button("Accedi");
		final Button regButton = new Button("Registrati");
		
		HorizontalPanel homePanel = new HorizontalPanel();
		homePanel.add(loginButton);
		homePanel.add(regButton);
		
		RootPanel.get().add(homePanel);
		RootPanel.get().setWidgetPosition(homePanel, 550, 300);

		
		regButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				showSchermataRegistrazione();		
				
			}
			
		});
		
		loginButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				showSchermataLogin();
			}
		});
	}	

	
	private void showSchermataRegistrazione() {
		SchermataRegistrazione rb = new SchermataRegistrazione(RootPanel.get(), electionService, this);
		rb.show();
	}
	
	private void showSchermataAmministratore() {
		SchermataAmministratore sa = new SchermataAmministratore(RootPanel.get(), electionService,this);
		sa.show();
	}
	
	
	
	private void showSchermataLogin() {
		RootPanel.get().clear();
		final HorizontalPanel usernamePanel = new HorizontalPanel();
		final TextBox usernameTextBox = new TextBox();
		usernameTextBox.getElement().setPropertyString("placeholder", "Username");
		usernamePanel.add(usernameTextBox);
		
		final HorizontalPanel passwordPanel = new HorizontalPanel();
		final PasswordTextBox passwordTextBox = new PasswordTextBox();
		passwordTextBox.getElement().setPropertyString("placeholder", "*******");
		passwordPanel.add(passwordTextBox);
		
		final VerticalPanel formPanel = new VerticalPanel();
		formPanel.add(usernamePanel);
		formPanel.add(passwordPanel);
		
		RootPanel.get().add(formPanel);
		RootPanel.get().setWidgetPosition(formPanel, 650, 300);
		
		final HorizontalPanel buttonsPanel = new HorizontalPanel();
		final Button cittadinoButton = new Button("Sono un Cittadino");
		final Button funzionarioButton = new Button("Sono un Funzionario Comunale");
		final Button adminButton = new Button("Sono un Admin");
		
		final Button esci = new Button("Esci");
		esci.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				onModuleLoad();		
			}		
		});
		
		
		buttonsPanel.add(cittadinoButton);
		buttonsPanel.add(funzionarioButton);
		buttonsPanel.add(adminButton);
		buttonsPanel.add(esci);
		
		RootPanel.get().add(buttonsPanel);
		RootPanel.get().setWidgetPosition(buttonsPanel, 530, 450);
		
		cittadinoButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				final String username = usernameTextBox.getText();
				final String password = passwordTextBox.getText();
				
				AsyncCallback<Void> callback = new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						Window.alert( caught.toString());
					}
					@Override
					public void onSuccess(Void result) {
						Window.alert("Accesso effettuato come cittadino");
					}	
				};
				electionService.loginCittadino(username, password, callback);	
			}
			
		});
		
		
		adminButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				String username = usernameTextBox.getText();
				String password = passwordTextBox.getText();
				AsyncCallback<Void> callback = new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						Window.alert( caught.toString() );		
					}
					@Override
					public void onSuccess(Void r) {
						showSchermataAmministratore();
					}
				};
				electionService.loginAdmin(username, password, callback);
			}
			
		});
		
		
	}

}
