package uni.sweng.progetto.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("election")
public interface ElectionService extends RemoteService {
	void loginCittadino(String username, String password) throws IllegalArgumentException;
	void loginAdmin(String username, String password) throws IllegalArgumentException;
	String registrazione(String nome, String cognome, String username, String password, String telefono,
			String email, String codiceFiscale, String indirizzoDomicilio, String idDocumentoIdentita) throws IllegalArgumentException;
	String nominaFunzionario(String username) throws IllegalArgumentException;
}
