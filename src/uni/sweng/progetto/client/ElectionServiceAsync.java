package uni.sweng.progetto.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface ElectionServiceAsync {
	void loginCittadino(String username, String password, AsyncCallback<Void> callback);
	void loginAdmin(String username, String password, AsyncCallback<Void> callback);
	void registrazione(String nome, String cognome, String username, String password, 
			String telefono, String email, String codiceFiscale, String indirizzoDomicilio, 
			String idDocumentoIdentita, AsyncCallback<String> callback);

	void nominaFunzionario(String username, AsyncCallback<String> callback);
}
