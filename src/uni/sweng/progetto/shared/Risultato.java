package uni.sweng.progetto.shared;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Risultato implements IsSerializable {
		
	
	
	private ArrayList<String> nomiLista;
	private ArrayList<Integer> votiLista;
	private ArrayList<String> nomiPreferenze;
	private ArrayList<Integer> votiPreferenze;
	
	public Risultato(){
		
	}
	
	
	public Risultato(	ArrayList<String> nomiLista,
						ArrayList<String> nomiPreferenze,
						ArrayList<Integer> votiLista,
						ArrayList<Integer> votiPreferenze) 
	{
		this.nomiLista = nomiLista;
		this.nomiPreferenze = nomiPreferenze;
		this.votiLista = votiLista;
		this.votiPreferenze = votiPreferenze;
	}

	public ArrayList<String> getNomiLista() {
		ArrayList<String> a  = this.nomiLista;
		return (a);
	}

	public ArrayList<Integer> getVotiListaPresi() {
		ArrayList<Integer> s = this.votiLista;
		return s;
	}

	public ArrayList<String> getNomiPreferenze() {
		ArrayList<String> a  = this.nomiPreferenze;
		return (a);
	}

	public ArrayList<Integer> getVotiPreferenzePresi() {
		ArrayList<Integer> s = this.votiPreferenze;
		return s;
	}


}
