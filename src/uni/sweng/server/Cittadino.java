package uni.sweng.server;

import java.io.Serializable;
import java.util.UUID;

/** Un Cittadino è un utente generico loggato al sistema/sito*/
public class Cittadino extends UtenteGenerico implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private String username;
	private String password;
	private String telefono;
	private String email;
	private String codiceFiscale;
	private String indirizzoDomicilio;
	private String idDocumentoIdentita;
	
	public Cittadino(){
	}
	
	public Cittadino(String nome, String cognome) {
		super(nome, cognome);
	}
	
	public Cittadino(String nome, String cognome, String username, String password, String telefono, String email, String codiceFiscale, String indirizzoDomicilio, String idDocumentoIdentita){
		super(nome,cognome);
		this.username = username;
		this.password = password;
		this.telefono = telefono;
		this.email = email;
		this.codiceFiscale = codiceFiscale;
		this.indirizzoDomicilio = indirizzoDomicilio;
		this.idDocumentoIdentita = idDocumentoIdentita;
	}
	
	@Override
	public String toString() {
		return super.toString();
	}
	
	public String getUsername() {
		return this.username;
	}
	
	public String getPassword() {
		return this.password;
	}
	
	//Necessario
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Cittadino))
			return false;
		else {
			Cittadino u = (Cittadino) o;
			if (u.getUsername().equals(this.getUsername()))
				return true;
			else 
				return false;
		}
	}

}

