package uni.sweng.server;

import java.io.Serializable;

public class UtenteGenerico implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2444333162439580104L;
	private String nome;
	private String cognome;
	
	public UtenteGenerico(){
		this.nome = null;
		this.cognome = null;
	}
	public UtenteGenerico(String nuovoNome, String nuovoCognome){
		this.nome = nuovoNome;
		this.cognome = nuovoCognome;
	}
	
	/*Restituisce il nome dell'utente generico*/
	public String getNome(){
		return nome;
	}
	/*Restituisce il cognome dell'utente generico*/
	public String getCognome(){
		return cognome;
	}
	
	//Necessario
	public String toString() {
		return "[ " + nome + " "  + cognome +"]";
	}
	

}

