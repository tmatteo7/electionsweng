package uni.sweng.shared;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;

public class ListaElettorale implements IsSerializable, Serializable, Cloneable {
	private String nomePresentatore;
	private String nome;
	private Integer IDElezione;
	private String simboloDesc;
	private String candidatoSindaco;
	private ArrayList<String> listaCandidati = new ArrayList<String>();
	private String status;

	
	public ListaElettorale(){
		
	}
	public ListaElettorale(String nomePresentatore, String nome, Integer IDElezione, String simboloDesc, String candidatoSindaco){
		this.nomePresentatore = nomePresentatore;
		this.nome = nome;
		this.IDElezione = IDElezione;
		this.simboloDesc = simboloDesc;
		this.candidatoSindaco = candidatoSindaco;
		this.status = "waiting";
	}
	
	/**
	 * Questo metodo permette l'aggiunta di un membro alla lista.
	 * @param nomeMembro
	 */
	public void aggiungiCandidato(String membro){
		if (this.candidatoSindaco.equals(membro)) {
			throw new IllegalArgumentException("Non si può candidare il sindaco in lista");
		} 
		else {
		this.listaCandidati.add(membro);	
		}
	}
	
	public Integer getIDElezione() {
		Integer id = this.IDElezione;
		return id;
	}
	public String getSimbolo() {
		return this.simboloDesc;
	}
	public String getNome() {
		return this.nome;
	}
	public String getSindaco() {
		return this.candidatoSindaco;
	}
	
	public ArrayList<String> getCandidati() {
		return listaCandidati;
	}
	
	public String getStatus() {
		return status;
	}
	
	public String getPresentatore() {
		return this.nomePresentatore;
	}
	
	public void approva() {
		this.status = "approved";
	}
	public void rifiuta() {
		this.status = "refused";
	}
	
	public String toString() {
		String ret = "IDElezione: " + this.getIDElezione() + "\n Nome: " + this.nome  
				+ "status: " + this.getStatus() + "\n Candidato sindaco: "+ this.getSindaco() +  "\n Lista candidati: [ \n	" ;
		for (String c : this.getCandidati()) {
			ret += "\n - " + c;
		}
		ret+= "]";
		return ret;
	}
	
	public Object clone() {
		ListaElettorale le = new ListaElettorale();
		le.candidatoSindaco = new String(this.candidatoSindaco);
		le.IDElezione = new Integer(this.IDElezione);
		le.listaCandidati = ((ArrayList<String>) this.listaCandidati.clone());
		le.nome = new String(this.nome);
		le.nomePresentatore = new String(this.nomePresentatore);
		le.simboloDesc = new String (this.simboloDesc);
		le.status = new String (this.status);
		return le;
	}
	
}
