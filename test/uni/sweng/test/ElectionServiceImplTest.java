package uni.sweng.test;

import java.util.Random;

import org.junit.Test;

import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

import uni.sweng.client.ElectionService;
import uni.sweng.client.ElectionServiceAsync;
import uni.sweng.server.Cittadino;
import uni.sweng.server.ElectionServiceImpl;
import java.util.ArrayList;
import java.util.Date;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.google.gwt.core.client.GWT;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;


public class ElectionServiceImplTest extends GWTTestCase {
	
	private ElectionServiceAsync Electionservice;
	private ServiceDefTarget target;
	
	private void setUpServer() {
		Electionservice = GWT.create(ElectionService.class);
		target = (ServiceDefTarget) Electionservice;
		target.setServiceEntryPoint(GWT.getModuleBaseURL() + "uni/sweng/electionservice");
		delayTestFinish(10000);
	}
	
	
	
	// test 0 registrazione
	@Test
	public void testRegistrazione() {
		setUpServer();
		
		//clear
			
		Random ran = new Random();
		ran.setSeed(System.currentTimeMillis());
		String username = "" + ran.nextFloat();
		
		Electionservice.registrazione("nome", "cognome", username, "password", "telefono",
			"email", "codiceFiscale", "indirizzoDomicilio", "idDocumentoIdentita", new TrueCallback());
	}
	
	
	// test 1 login
	
	@Test
	public void testLogin() {
		setUpServer();
		Electionservice.loginCittadino("admin", "admin", new TrueLCallback());
	}
	
	// test 2 nomina di un funzionario comunale 
	
	@Test
	public void testNominaFunzionario() {
		setUpServer();

		Random ran = new Random();
		ran.setSeed(System.currentTimeMillis());
		String username = "" + ran.nextFloat();
		Electionservice.registrazione("nome", "cognome", username, "password", "teelfono",
			"email", "codiceFiscale", "indirizzoDomicilio", "idDocumentoIdentita", new xCallback() );
		Electionservice.nominaFunzionario(username, new TrueCallback());
	}
	
	
	
	@Override
	public String getModuleName() {
		return "uni.sweng.prX.ElectionServiceImplTestJUnit";
	}


	
	private class xCallback implements AsyncCallback<String>{

		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onSuccess(String result) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
	private class TrueLCallback implements AsyncCallback<Void> {
		@Override
		public void onFailure(Throwable caught) {
			fail("Errore: " + caught.getMessage());			
		}
		
		@Override
		public void onSuccess(Void result) {
			finishTest();
		}	
	}
	
	
	
	private class TrueCallback implements AsyncCallback<String> {
		@Override
		public void onFailure(Throwable caught) {
			fail("Errore: " + caught.getMessage());
		}
		
		@Override
		public void onSuccess(String result) {
			assertNotNull(result);
			finishTest();
		}	
	}
	
	
	
	


}


